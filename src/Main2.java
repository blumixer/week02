import java.util.Scanner;

public class Main2
{
    public static void main(String[] args) {
        Scanner scannerIn = new Scanner(System.in);
        double a = 0.0;
        double b = 0.0;
        double c = 0.0;
        double x1 = 0.0;
        double x2 = 0.0;

        System.out.print("Enter a value for [a]: ");
        a = scannerIn.nextDouble();

        System.out.print("Enter a value for [b]: ");
        b = scannerIn.nextDouble();

        System.out.print("Enter a value for [c]: ");
        c = scannerIn.nextDouble();

        if (a == 0.0) {
            System.out.println("Cannot solve the quadratic formula.");
        } else {
            x1 = (-b + Math.sqrt(b * b - (4.0 * a * c))) / (2 * a);
            x2 = (-b - Math.sqrt(b * b - (4.0 * a * c))) / (2 * a);

            System.out.println("The value for x1 is " + x1);
            System.out.println("The value for x2 is " + x2);
        }
    }
}