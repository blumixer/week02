import java.util.Scanner;
import java.lang.String;

public class Main5
{
    public static double convertFahrenheitToCelsius(double fahrenheitTemperature)
    {
        return (fahrenheitTemperature - 32.0) * 5.0 / 9.0;
    }

    public static double convertCelsiusToFahrenheit(double celsiusTemperature)
    {
        double fahrenheit = 0.0;
        fahrenheit = (celsiusTemperature * 9.0) / 5.0 + 32.0;
        return fahrenheit;
    }

    public static int convertCelsiusToFahrenheit(int celsiusTemperature)
    {
        int fahrenheit = 0;
        fahrenheit = (celsiusTemperature * 9) / 5 + 32;
        return fahrenheit;
    }

    public static void main(String[] args)
    {
        Scanner scannerIn = new Scanner(System.in);
        double fahrenheit = 0.0;
        double celsius = 0.0;
        int count = 0;
        double temperatureSum = 0.0;
        double temperatureAverage = 0.0;
        string keepGoing = "N";

        do
        {
            System.out.print("Enter a fahrenheit temperature: ");
            fahrenheit = scannerIn.nextDouble();
            celsius = convertFahrenheitToCelsius(fahrenheit);
            temperatureSum += celsius;
            count++;
            System.out.print("Keep Going (Y/N)?");
            keepGoing = scannerIn.next();
        }
        while (keepGoing.equals ("Y")|| keepGoing.equals ("y"));

        temperatureAverage = temperatureSum / (double) count;
        System.out.println("Average celsius temperature is: " + temperatureAverage);
    }
}