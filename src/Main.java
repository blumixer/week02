import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        // declare and initialize all variables
        Scanner scannerIn = new Scanner(System.in);

        int temp = 42;

        double fahrenheit = 0.0;
        double fahrenheitDoubled = 0.0;

        // get input
        System.out.print("Enter Temperature: ");

        fahrenheit = scannerIn.nextDouble();
        // perform calculations
        fahrenheitDoubled = fahrenheit * 2.0;

        // display output

        System.out.println("Twice the fahrenheit value is: " + fahrenheitDoubled);
    }
}
