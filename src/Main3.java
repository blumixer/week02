import java.util.Scanner;

public class Main3
{
    public static double convertCelsiusToFahrenheit(double celsiusTemperature)
    {
        double fahrenheit = 0.0;
        fahrenheit = (celsiusTemperature * 9.0) / 5.0 + 32.0;
        return fahrenheit;
    }

    public static int convertCelsiusToFahrenheit(int celsiusTemperature)
    {
        int fahrenheit = 0;
        fahrenheit = (celsiusTemperature * 9) / 5 + 32;
        return fahrenheit;
    }

    public static double convertCelsiusToKelvin(double celsiusTemperature)
    {
        double kelvin = 0.0;
        kelvin = celsiusTemperature + 273;
        return kelvin;
    }

    public static void main(String[] args)
    {
        Scanner scannerIn = new Scanner(System.in);
        double fahrenheit = 0.0;
        double celsius = 0.0;
        double kelvin = 0.0;

        int fahrenheitInt = 0;
        int celsiusInt = 0;
        int kelvinInt = 0;

        System.out.print("Enter a celsius temperature: ");
        celsius = scannerIn.nextDouble();
        celsiusInt = scannerIn.nextInt();

        // fahrenheit = convertCelsiusToFahrenheit(celsius);
        fahrenheitInt = convertCelsiusToFahrenheit(celsiusInt);
        kelvin = convertCelsiusToKelvin(celsius);


        System.out.println("Fahrenheit temperature is: " + fahrenheit);
        System.out.println("Kelvin temperature is: " + kelvin);
    }
}